$('#menuToggle').on('click', function () {
    $(this).toggleClass('menuToggle--opened');
});

$.ajax({
    url: '/assets/json/services.json',
    dataType: 'json',
    method: 'GET',
}).done(function (data) {
    $.each(data.services, function (i, service) {
        $('#services').append('<div><h2 class="mb-4">' + service.title + '</h2></div>');
    });
});
